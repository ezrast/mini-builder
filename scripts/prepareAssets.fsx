#r "nuget: SixLabors.ImageSharp, 2.1.2"
#r "nuget: Thoth.Json.Net"
#load "../app/src/Attribute.fs"
#load "../app/src/Talent.fs"

open SixLabors.ImageSharp
open SixLabors.ImageSharp.Processing
open Thoth.Json.Net

let src, dest =
  match System.Environment.GetCommandLineArgs() with
  | [| _; _; src; dest |] -> src, dest
  | _ -> failwith "Requires `src` and `dest` arguments"

type RawTalent =
  {
    TalentName: string
    Key: string
    Type: int
    Position: int
    tier: int
    maxLevel: int
  }

let extraDecoders =
  Extra.empty
  |> Extra.withCustom Attribute.T.encoder Attribute.T.decoder

// fsi crashes if I don't either namespace this or change the field names
// to something that doesn't collide with Talent.T 🤷
module TalentFixup =
  type T =
    {
      perRank: List<string * List<float>>
      thresholds: Option<List<int * string * List<float>>>
      sacrifice: Option<List<string * List<float>>>
      unmodeled: Option<string>
      iconPath: Option<string>
      iconCrop: Option<int * int * int * int>
    }


  let fixupJson =
    System.IO.File.ReadAllText($"{__SOURCE_DIRECTORY__}/talent_fixups.json")

  let fixups =
    let myDecoder =
      Decode.Auto.generateDecoderCached<Map<string, T>> (caseStrategy = CamelCase, extra = extraDecoders)
    match Decode.fromString myDecoder (fixupJson) with
    | Ok ok -> ok
    | Error err -> failwith $"Could not decode fixup json: {err}"

module TalentDescription =
  type T =
    {
      description: string
      attributes: Option<List<Attribute.T>>
    }

  let descriptionJson =
    System.IO.File.ReadAllText($"{__SOURCE_DIRECTORY__}/talent_descriptions.json")

  let descriptions =
    match Decode.Auto.fromString<Map<string, T>> (descriptionJson, extra = extraDecoders) with
    | Ok ok -> ok
    | Error err -> failwith $"Could not decode description json: {err}"

let splitKV (str: string) =
  let idx = str.IndexOf("=")
  if idx < 0 then
    System.Console.WriteLine($"Bad string: {str}")
  let key = str.Substring(0, idx)
  let value = str.Substring(idx + 1)
  (key, value)

let talentNames =
  System.IO.File.ReadAllLines(src + "/Assets/Resources/local/en_us/TALENT.txt")
  |> Array.toSeq
  |> Seq.filter ((<>) "")
  |> Seq.filter ((<>) "END")
  |> Seq.map splitKV
  |> Map.ofSeq

let talentJson =
  System.IO.File.ReadAllText(
    src
    + "/Assets/Resources/gamedata/talent/TalentData.json"
  )

let raw =
  match Decode.Auto.fromString<Map<string, List<RawTalent>>> (talentJson) with
  | Ok (ok) -> ok
  | Error (err) -> failwith $"Could not decode json: {err}"

let list =
  match raw |> Map.tryFind ("Talents") with
  | Some (some) -> some
  | None -> failwith "Talents not found in talents file"

let saferWrite destPath srcBytes =
  if System.IO.File.Exists(destPath) then
    if srcBytes <> System.IO.File.ReadAllBytes destPath then
      failwith $"Asset differs from existing file {destPath}"
  else
    System.IO.File.WriteAllBytes(destPath, srcBytes)

let saferCopy destPath srcPath =
  let bytes = System.IO.File.ReadAllBytes(srcPath)
  saferWrite destPath bytes

System.IO.Directory.CreateDirectory($"{dest}/talents")

let rec loop rawTalents talents errors idx =
  match rawTalents with
  | rawTalent :: rest ->
    let name =
      talentNames
      |> Map.tryFind (rawTalent.TalentName)
      |> Option.defaultValue ("")
    match TalentFixup.fixups |> Map.tryFind (name) with
    | None -> loop rest talents errors idx
    | Some (fixup) ->
      let iconSrc =
        fixup.iconPath
        |> Option.defaultValue ($"Assets/Resources/image/talents/{rawTalent.Key}.png")
      let iconDest = $"talents/{rawTalent.Key}.png"
      let errors =
        try
          match fixup.iconCrop with
          | None ->
            saferCopy $"{dest}/{iconDest}" $"{src}/{iconSrc}"
            errors
          | Some (xx, yy, dx, dy) ->
            use img = Image.Load($"{src}/{iconSrc}")
            img.Mutate(fun img -> ignore (img.Crop(Rectangle(xx, yy, dx, dy))))
            img.SaveAsPng("/tmp/minibuilder_icon.png")
            saferCopy $"{dest}/{iconDest}" $"/tmp/minibuilder_icon.png"
            errors
        with
        | _ as err -> err :: errors

      let fixupToEffect rawEffect =
        let mutable perRankStats = Map.empty
        let mutable perRankDescriptions = []
        for (str, amounts) in rawEffect do
          let descr: TalentDescription.T =
            match TalentDescription.descriptions |> Map.tryFind str with
            | Some (some) -> some
            | None -> failwith $"No description matching {str}"
          let attrs =
            descr.attributes
            |> Option.defaultWith (fun () ->
              match (Attribute.T.caseMap |> Map.tryFind str) with
              | Some attr -> [ attr ]
              | None -> failwith $"No attribute: {str}")
            |> List.toSeq
          Seq.iter2 (fun x y -> perRankStats <- perRankStats |> Map.add x y) attrs amounts
          let mungedDescr =
            descr
              .description
              .Replace("[]", $"[{str}]")
              .Replace("[+]", $"[+{str}]")
          perRankDescriptions <- mungedDescr :: perRankDescriptions
        {
          Talent.Stats = perRankStats
          Talent.Descriptions = (perRankDescriptions |> List.rev)
        }

      let perRank = fixupToEffect fixup.perRank
      let thresholds =
        fixup.thresholds
        |> Option.defaultValue ([])
        |> List.groupBy (fun (rank, _, _) -> rank)
        |> List.map (fun (rank, list) ->
          let effectList =
            list
            |> List.map (fun (_, descr, amounts) -> descr, amounts)
          rank, fixupToEffect effectList)
      let sacrifice = fixupToEffect (fixup.sacrifice |> Option.defaultValue ([]))

      let newTalent: Talent.T =
        {
          Name = name
          PerRank = perRank
          Thresholds = thresholds
          Sacrifice = sacrifice
          MaxRanks = rawTalent.maxLevel
          Class = rawTalent.Type
          Tier = rawTalent.tier - 1 // convert 1-based to 0-based
          Column = rawTalent.Position
          Icon = iconDest
          ExportIndex = idx
        }

      loop rest (newTalent :: talents) errors (idx + 1)

  | [] -> talents |> List.rev, errors

let talentsSortedByKey = list |> List.sortBy (fun tal -> tal.Key)
let talents, errors = loop talentsSortedByKey [] [] 0

for error in errors do
  System.Console.WriteLine($"Error: {error}")

let destPath = dest + "/talents/talents.json"
let outJson = Encode.Auto.toString<List<Talent.T>> (0, talents)
saferWrite destPath (System.Text.Encoding.UTF8.GetBytes(outJson))
