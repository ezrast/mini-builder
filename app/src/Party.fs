module Party

open type Attribute.T

type T(attributes: List<string * Attribute.StatStick>) =
  let totalAttributes =
    Attribute.merge (
      attributes
      |> List.map (fun (_name, stick) -> stick)
    )

  member this.Attribute(attr) =
    Attribute.getAttribute attr totalAttributes

  member this.GetAttributeBreakdown(attr: Attribute.T) =
    attributes
    |> Seq.ofList
    |> Seq.map (fun (name, stick) -> (name, Attribute.getAttribute attr stick))
    |> Seq.filter (fun (_name, amount) -> amount <> 0.0)

let baseAttributes: Attribute.StatStick =
  [
    MaximumMana, 1000.0
    ManaRegeneration, 8.5
    CriticalHealChance, 0.01
    CriticalHealMultiplier, 1.4

    PartyCriticalStrikeChance, 0.01
    PartyCriticalStrikeDamage, 1.4

    TankAttackSpeed, 0.83
    TankMaximumHealth, 550.5
    TankBlockChance, 0.01
    TankPhysicalDamageBase, 17.0

    BerzerkerAttackSpeed, 0.33
    BerzerkerMaximumHealth, 320.0
    BerzerkerPhysicalDamageBase, 40.0

    HealerAttackSpeed, 1.0
    HealerMaximumHealth, 240.0
    HealerPhysicalDamageBase, 12.5

    RangerAttackSpeed, 1.33
    RangerMaximumHealth, 210.0
    RangerDodgeChance, 0.01
    RangerPhysicalDamageBase, 14.0
  ]
  |> Map.ofList

let tankEffect: Attribute.StatStick = [ TankPhysicalResist, 0.2 ] |> Map.ofList

let perLevelAttributes: Attribute.StatStick =
  [
    Healpower, 11.0

    TankMaximumHealth, 127.5
    TankPhysicalDamageBase, 11.0

    BerzerkerMaximumHealth, 110.5
    BerzerkerPhysicalDamageBase, 30.0

    HealerMaximumHealth, 90.1
    HealerPhysicalDamageBase, 9.5

    RangerMaximumHealth, 85.0
    RangerPhysicalDamageBase, 14.0
  ]
  |> Map.ofList

let make level namedAttributes =
  T(
    [
      ("Base", baseAttributes)
      ("Tank Effect", tankEffect)
      ($"Level {level + 1}", Attribute.stack perLevelAttributes level)
    ]
    @ namedAttributes
  )
