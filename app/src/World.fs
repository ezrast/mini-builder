module World

open type Attribute.T

[<AbstractClass>]
type private Case<'A>() =
  abstract Derive: ('A -> Case<'B>) -> Case<'B>
  abstract ToSeq: unit -> seq<List<string> * float * 'A>
  abstract ToSeqInner: List<string> * float -> seq<List<string> * float * 'A>
  default this.ToSeq() = this.ToSeqInner([], 1.0)

type private Single<'A>(derived) =
  inherit Case<'A>()
  override this.Derive(fn: 'A -> Case<'B>) = fn derived
  override this.ToSeqInner(supers: List<string>, fraction: float) = seq { supers, fraction, derived }
  member this.AsCase() = this :> Case<'A>

type private Split<'A>(cases: List<float * string * Case<'A>>) =
  inherit Case<'A>()
  let denominator =
    cases
    |> List.fold (fun acc (numerator, _, _) -> acc + numerator) 0.0
  override this.Derive(fn: 'A -> Case<'B>) =
    Split(
      cases
      |> List.map (fun (incidenceRate, name, altCase) -> (incidenceRate, name, altCase.Derive fn))
    )
    :> Case<'B>
  override this.ToSeqInner(supers: List<string>, fraction: float) =
    seq {
      for (numerator, name, case) in cases do
        yield! case.ToSeqInner(name :: supers, fraction * numerator / denominator)
    }
  member this.AsCase() = this :> Case<'A>

type private DerivedStats =
  {| healerPhysicalTotal: float
     healerAutoPhysicalDPS: float |}

let toString (derived: DerivedStats) =
  $"healerPhysicalTotal: {derived.healerPhysicalTotal}\n"
  + $"healerAutoPhysicalDPS: {derived.healerAutoPhysicalDPS}\n"

type T(party: Party.T) =
  let world = Single(()) :> Case<unit>

  let world =
    world.Derive (fun (_derived: unit) ->
      let healerPhysicalTotal =
        (party.Attribute(HealerPhysicalDamageBase))
        * (1.0 + party.Attribute(HealerPhysicalDamageIncr))
      Single(
        {|
          healerPhysicalTotal = healerPhysicalTotal
        |}
      )
        .AsCase())

  let world =
    world.Derive (fun derived ->
      let noncrit =
        party.Attribute HealerAttackSpeed
        * derived.healerPhysicalTotal
      match party.Attribute HealerCriticalStrikeChance with
      | xx when xx <= 0.0 ->
        Single(
          {| derived with
              healerAutoPhysicalDPS = noncrit
          |}
        )
        :> Case<_>
      | xx when xx >= 1.0 ->
        Single(
          {| derived with
              healerAutoPhysicalDPS =
                noncrit
                * party.Attribute HealerCriticalStrikeDamage
          |}
        )
        :> Case<_>
      | xx ->
        let critCase =
          Single(
            {| derived with
                healerAutoPhysicalDPS =
                  noncrit
                  * party.Attribute HealerCriticalStrikeDamage
            |}
          )
          :> Case<_>
        let nonCritCase =
          Single(
            {| derived with
                healerAutoPhysicalDPS = noncrit
            |}
          )
          :> Case<_>

        Split([ xx, "crit", critCase; 1.0 - xx, "noncrit", nonCritCase ]) :> Case<_>)

  // Assert type
  let world: Case<DerivedStats> = world

  member this.ToString =
    world.ToSeq()
    |> Seq.map (fun (supers, rate, case) ->
      let heading = supers |> String.concat " "
      $"  %.1f{rate * 100.0}%%  {heading}\n{toString case}")
    |> String.concat "\n"
