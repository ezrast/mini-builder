module TalentGrid

open Elmish
open Fable.React
open Fable.React.Props

open type Attribute.DisplayType

let fmtFloat (value: float) =
  if value < 0.0 then
    $"−%.2f{-value}".TrimEnd('0').TrimEnd('.')
  else
    $"%.2f{value}".TrimEnd('0').TrimEnd('.')

let describeEffect (effect: Talent.Effect) =
  let printValue displayType value =
    seq {
      match displayType with
      | Flat ->
        yield
          span [ Class "attribute-value" ] [
            str <| fmtFloat value
          ]
      | Percent ->
        yield
          span [ Class "attribute-value" ] [
            str <| fmtFloat (value * 100.0)
          ]
        yield str "%"
    }
  let printSigned displayType value =
    seq {
      if value >= 0.0 then
        yield str "+"
        yield! printValue displayType value
      else
        yield str "−"
        yield! printValue displayType -value
    }
  let printUnsigned displayType value =
    seq {
      if value >= 0.0 then
        yield! printValue displayType value
      else
        yield str "−"
        yield! printValue displayType -value
    }

  let rec loop (fmtStr: string) =
    seq {
      match fmtStr.IndexOf("[") with
      | -1 -> yield str fmtStr
      | idxLeft ->
        match fmtStr.IndexOf("]") with
        | -1 -> ()
        | idxRight ->
          yield str (fmtStr.Substring(0, idxLeft))
          let attrName = fmtStr.Substring(idxLeft + 1, idxRight - idxLeft - 1)
          let attrName, print =
            if attrName.StartsWith("+") then
              attrName.Substring(1), printSigned
            else
              attrName, printUnsigned

          match Attribute.T.caseMap |> Map.tryFind attrName with
          | None -> ()
          | Some attrLabel ->
            match effect.Stats |> Map.tryFind attrLabel with
            | Some amount -> yield! print attrLabel.DisplayType amount
            | None -> yield str "-?-"

          let remainder = fmtStr.Substring(idxRight + 1)
          yield! loop remainder
    }
  effect.Descriptions |> List.map loop

type Model =
  {
    Talents: array<Talent.T>
    mutable PointsSpent: int
    Prereqs: array<int>
    Class: int
  }

  member this.ModifyRank(talent: Talent.T, delta: int) =
    Msg.UpdateTalentRanks(this.Class, talent.ExportIndex, delta)

  member this.AdjustPrereqs(tier, delta) =
    this.PointsSpent <- this.PointsSpent + delta
    let rec loop idx =
      if idx > tier then
        this.Prereqs.[idx] <- this.Prereqs.[idx] - delta
        loop (idx - 1)

    loop (Talent.TalentTiers - 1)

  member this.Reset() =
    this.Prereqs
    |> Array.iteri (fun idx _ -> this.Prereqs.[idx] <- 4 * idx)
    this.PointsSpent <- 0

  member this.RecalculatePrereqs(talentRanks: array<int>) =
    this.Prereqs
    |> Array.iteri (fun idx _ -> this.Prereqs.[idx] <- idx * 4)
    this.Talents
    |> Array.iter (fun tal -> this.AdjustPrereqs(tal.Tier, talentRanks.[tal.ExportIndex]))

  member this.Render(ranks: array<int>, dispatch) =
    let gridCells =
      [
        for tal in this.Talents do
          let rank = ranks.[tal.ExportIndex]
          let rankDescriptor =
            if rank = 0 then
              "lo"
            else if this.Prereqs.[tal.Tier] > 0 then
              "bad"
            else if rank = tal.MaxRanks then
              "hi"
            else
              "mid"

          let rankInfo heading (effect: Talent.Effect) =
            let elements = describeEffect effect
            match effect.Descriptions with
            | [] -> fragment [] []
            | [ _ ] ->
              if tal.MaxRanks = 1 then
                div
                  []
                  (elements
                   |> List.map (span [ Class "rank-attribute" ]))
              else
                div
                  []
                  (span [ Class "rank-label" ] [
                    str heading
                   ]
                   :: (elements
                       |> List.map (span [ Class "rank-attribute" ])))
            | _ ->
              fragment
                []
                ((div [ Class "rank-label" ] [
                    str heading
                  ])
                 :: [
                   yield!
                     elements
                     |> List.map (div [ Class "rank-attribute" ])
                 ])

          let tooltipInfo: ReactElement =
            div [ Class "tooltip" ] [
              header [] [ str tal.Name ]
              fragment [] [
                for rank, effect in tal.Thresholds do
                  rankInfo $"At rank {rank}: " effect
              ]
              rankInfo "Each rank: " tal.PerRank
              fragment
                []
                (if List.isEmpty tal.Sacrifice.Descriptions then
                   []
                 else
                   [ rankInfo "Sacrifice: " tal.Sacrifice ])
            ]

          fragment [] [
            div [
                  Tooltip.tipped
                  Class $"talent-cell {rankDescriptor}"
                  Style [ GridRow(tal.Tier + 1)
                          GridColumn(tal.Column + 1) ]
                  OnContextMenu(fun evt -> evt.preventDefault ())
                  OnMouseDown (fun evt ->
                    match evt.button with
                    | 0.0 when rank < tal.MaxRanks -> dispatch <| this.ModifyRank(tal, 1)
                    | 2.0 when rank > 0 -> dispatch <| this.ModifyRank(tal, -1)
                    | _ -> ())
                ] [
              img [ Src $"assets/{tal.Icon}" ]
              div [ Class "talent-rank" ] [
                span [ Class "talent-rank-current" ] [
                  str $"{rank}"
                ]
                str $" / {tal.MaxRanks}"
              ]
            ]
            tooltipInfo
          ]
      ]
    let tierCountCells =
      [
        for tier in 1 .. (Talent.TalentTiers - 1) do
          if this.Prereqs.[tier] > 0 then
            div [
                  Class($"talent-prereq locked")
                  Style [ GridRow(tier + 1)
                          GridColumn(Talent.TalentColumns + 1) ]
                ] [
              div [ Tooltip.tipped ] [
                str $"{this.Prereqs.[tier]}"
              ]
              div [ Class "tooltip" ] [
                str $"{this.Prereqs.[tier]} more talents in previous tiers required"
              ]
            ]
          else
            div [
                  Class($"talent-prereq ok")
                  Style [ GridRow(tier + 1)
                          GridColumn(Talent.TalentColumns + 1) ]
                ] [
              div [ Tooltip.tipped ] [
                str $"({-this.Prereqs.[tier]})"
              ]
              div [ Class "tooltip" ] [
                str $"Talent requirements met"
              ]
            ]
      ]
    div [ Class "talent-grid" ] [
      fragment [] gridCells
      fragment [] tierCountCells
    ]
