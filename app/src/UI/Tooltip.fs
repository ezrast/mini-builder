module Tooltip

open Fable.React.Props
open Browser.CssExtensions

let updateTooltipXY (evt: Browser.Types.MouseEvent) =
  let xx, yy = evt.clientX, evt.clientY
  let root = Browser.Dom.document.documentElement
  let offset = 20.0
  let (left), (right) =
    if xx > root.clientWidth / 2.0 then
      "auto", $"{root.clientWidth - xx + offset}px"
    else
      $"{xx + offset}px", "auto"
  let (top), (bottom) =
    if yy > root.clientHeight / 2.0 then
      "auto", $"{root.clientHeight - yy + offset}px"
    else
      $"{yy + offset}px", "auto"
  root.style.setProperty ("--tooltip-left", left)
  root.style.setProperty ("--tooltip-right", right)
  root.style.setProperty ("--tooltip-top", top)
  root.style.setProperty ("--tooltip-bottom", bottom)

// To turn an element into a tooltip, do all of:
// * set `Class "tooltip"` on the tip
// * add `tipped` to the tipped element
// * place the tip as a *sibling* following the tipped element
let tipped = (OnMouseEnter updateTooltipXY) :> IHTMLProp
