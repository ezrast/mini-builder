module StatsPanel

open Fable.React
open Fable.React.Props

open type Attribute.T
open type Msg.T

open type Attribute.DisplayType

let fmtValue (attr: Attribute.T) (value: float) =
  let value, trailer =
    match attr.DisplayType with
    | Flat -> value, " "
    | Percent -> value * 100.0, "%"

  let whole, fraction =
    match $"%.2f{value}".Split('.') with
    | [| whole; fraction |] -> whole, fraction
    | _ -> failwith "Unreachable"

  fragment [] [
    str $"{whole}."
    span [ Class "fraction" ] [
      str fraction
    ]
    str trailer
  ]

let render (talentPoints: int) (party: Party.T) dispatch =
  let makeAttributeDisplay attr =
    let breakdown =
      [
        for (name, amount) in party.GetAttributeBreakdown(attr) do
          li [] [
            str $"{name}: {amount.ToString()}"
          ]
      ]
    let content = fmtValue attr (party.Attribute attr)
    match breakdown with
    | [] ->
      div [ Class "attribute-line-value attribute-line-zero" ] [
        content
      ]
    | _ ->
      fragment [] [
        div [ Tooltip.tipped; Class "attribute-line-value" ] [
          content
        ]
        div [ Class "tooltip" ] [
          for (name, amount) in party.GetAttributeBreakdown(attr) do
            div [ Class "attribute-line" ] [
              div [ Class "attribute-line-label" ] [
                str name
              ]
              div [ Class "attribute-line-value" ] [
                fmtValue attr amount
              ]
            ]
        ]
      ]

  let statLine text attr1 attr2 =
    match party.Attribute(attr1), party.Attribute(attr2) with
    | 0.0, 0.0 -> fragment [] []
    | _ ->
      div [ Class "attribute-line" ] [
        div [ Class "attribute-line-label" ] [
          str text
        ]
        makeAttributeDisplay attr1
        makeAttributeDisplay attr2
      ]

  let statLineFlat text attr =
    match party.Attribute(attr) with
    | 0.0 -> fragment [] []
    | _ ->
      div [ Class "attribute-line" ] [
        div [ Class "attribute-line-label" ] [
          str text
        ]
        makeAttributeDisplay attr
      ]

  let statLineIncr text attr =
    match party.Attribute(attr) with
    | 0.0 -> fragment [] []
    | _ ->
      div [ Class "attribute-line" ] [
        div [ Class "attribute-line-label" ] [
          str text
        ]
        div [ Class "attribute-line-value" ] [
          str " "
        ]
        makeAttributeDisplay attr
      ]

  div [ Class "attribute-panel" ] [
    div [ Class "attribute-header" ] [
      str $"Talent points: {talentPoints} / 99"
      div [ ] [
        button [ Class "reset"; OnMouseDown(fun _evt -> dispatch (ResetTalents)) ] [
          str "Reset Talents"
        ]
        button [ Class "reset"; OnMouseDown(fun _evt -> dispatch (ResetClass)) ] [
          str "Reset Class"
        ]
      ]
    ]
    h2 [] [ str "Attributes" ]
    details [ Open true ] [
      summary [] [ str "General" ]
      statLine "Healpower:" Healpower HealpowerIncr
      statLineIncr "Healing Done:" HealingIncr
      statLineIncr "Shielding Done:" HealingIncr
      statLineIncr "Cast Speed:" CastSpeedIncr
      statLineFlat "Critical Heal Chance:" CriticalHealChance
      statLineFlat "Critical Heal Chance Per Aura:" CriticalHealChancePerAura
      statLineFlat "Critical Heal Multiplier:" CriticalHealMultiplier
      statLineFlat "Critical Heal Multiplier Per Aura:" CriticalHealMultiplierPerAura
      statLine "Maximum Mana:" MaximumMana MaximumManaIncr
      statLine "Mana Regeneration:" ManaRegeneration ManaRegenerationIncr
      statLineFlat "Mana Cost of Skills:" ManaCostOfSkills
      statLineFlat "Mana Cost of Channeled Skills:" ManaCostOfChanneledSkills
      statLineIncr "Area of Effect:" AreaOfEffectIncr
      statLineIncr "Aura Effect:" AuraEffect
      statLineIncr "Area Mana Reduction:" AuraManaReductionIncr
      statLineIncr "Movement Speed:" MovementSpeedIncr
    ]
    details [ Open true ] [
      summary [] [ str "Party" ]
      statLineIncr "Attack Speed:" PartyAttackSpeedIncr
      statLineFlat "Critical Strike Chance:" PartyCriticalStrikeChance
      statLineFlat "Critical Strike Damage:" PartyCriticalStrikeDamage
      statLineIncr "Base Damage:" PartyDamageBaseIncr
      statLineIncr "Total Damage:" PartyDamageIncr
      statLineIncr "Base Elemental Damage:" PartyElementalDamageBaseIncr
      statLineIncr "Total Elemental Damage:" PartyElementalDamageIncr
      statLine "Physical Damage:" PartyPhysicalDamageBase PartyPhysicalDamageBaseIncr
      statLineIncr "Total Physical Damage:" PartyPhysicalDamageIncr
      statLine "Ice Damage:" PartyIceDamageBase PartyIceDamageBaseIncr
      statLineIncr "Total Ice Damage:" PartyIceDamageIncr
      statLine "Fire Damage:" PartyFireDamageBase PartyFireDamageBaseIncr
      statLineIncr "Total Fire Damage:" PartyFireDamageIncr
      statLine "Lightning Damage:" PartyLightningDamageBase PartyLightningDamageBaseIncr
      statLineIncr "Total Lightning Damage:" PartyLightningDamageIncr
      statLine "Nemesis Damage:" PartyNemesisDamageBase PartyNemesisDamageBaseIncr
      statLineIncr "Total Nemesis Damage:" PartyNemesisDamageIncr
      statLineIncr "More Damage per Aura:" PartyDamagePerAuraMore
      statLineIncr "More Damage While Shielded:" PartyShieldedDamageMore

      statLine "Maximum Health:" PartyMaximumHealth PartyMaximumHealthIncr
      statLineIncr "Less Damage Taken While Shielded:" PartyShieldedDamageTakenLess
      statLineFlat "Health Regeneration:" PartyHealthRegeneration
      statLineIncr "Healing Received:" PartyHealingReceivedIncr
      statLineFlat "All Resistance:" PartyAllResist
      statLineFlat "Elemental Resistance:" PartyElementalResist
      statLineFlat "Physical Resistance:" PartyPhysicalResist
      statLineFlat "Fire Resistance:" PartyFireResist
      statLineFlat "Ice Resistance:" PartyIceResist
      statLineFlat "Lightning Resistance:" PartyLightningResist
      statLineFlat "Nemesis Resistance:" PartyNemesisResist
      statLineIncr "Damage Taken:" PartyDamageTakenIncr
      statLineFlat "Block Chance:" PartyBlockChance
      statLineFlat "Dodge Chance:" PartyDodgeChance
      statLineIncr "Movement Speed:" PartyMovementSpeedIncr
    ]
    details [ Open true ] [
      summary [] [ str "Tank" ]
      statLine "Attack speed:" TankAttackSpeed TankAttackSpeedIncr
      statLineFlat "Critical Strike Chance:" TankCriticalStrikeChance
      statLineFlat "Critical Strike Damage:" TankCriticalStrikeDamage
      statLineIncr "Base Damage:" TankDamageBaseIncr
      statLineIncr "Total Damage:" TankDamageIncr
      statLineIncr "Base Elemental Damage:" TankElementalDamageBaseIncr
      statLineIncr "Total Elemental Damage:" TankElementalDamageIncr
      statLine "Physical Damage:" TankPhysicalDamageBase TankPhysicalDamageBaseIncr
      statLineIncr "Total Physical Damage:" TankPhysicalDamageIncr
      statLine "Ice Damage:" TankIceDamageBase TankIceDamageBaseIncr
      statLineIncr "Total Ice Damage:" TankIceDamageIncr
      statLine "Fire Damage:" TankFireDamageBase TankFireDamageBaseIncr
      statLineIncr "Total Fire Damage:" TankFireDamageIncr
      statLine "Lightning Damage:" TankLightningDamageBase TankLightningDamageBaseIncr
      statLineFlat "Lightning Damage per Level:" TankLightningDamageBasePerLevel
      statLineIncr "Total Lightning Damage:" TankLightningDamageIncr
      statLine "Nemesis Damage:" TankNemesisDamageBase TankNemesisDamageBaseIncr
      statLineIncr "Total Nemesis Damage:" TankNemesisDamageIncr

      statLine "Maximum Health:" TankMaximumHealth TankMaximumHealthIncr
      statLineFlat "Health Regeneration:" TankHealthRegeneration
      statLineFlat "Elemental Resistance:" TankElementalResist
      statLineFlat "Physical Resistance:" TankPhysicalResist
      statLineFlat "Fire Resistance:" TankFireResist
      statLineFlat "Ice Resistance:" TankIceResist
      statLineFlat "Lightning Resistance:" TankLightningResist
      statLineFlat "Nemesis Resistance:" TankNemesisResist
      statLineFlat "Block Chance:" TankBlockChance
      statLineFlat "Dodge Chance:" TankDodgeChance
      statLineIncr "Movement Speed:" TankMovementSpeedIncr
    ]
    details [ Open true ] [
      summary [] [ str "Berzerker" ]
      statLine "Attack speed:" BerzerkerAttackSpeed BerzerkerAttackSpeedIncr
      statLineFlat "Critical Strike Chance:" BerzerkerCriticalStrikeChance
      statLineFlat "Critical Strike Damage:" BerzerkerCriticalStrikeDamage
      statLineIncr "Base Damage:" BerzerkerDamageBaseIncr
      statLineIncr "Total Damage:" BerzerkerDamageIncr
      statLineIncr "Base Elemental Damage:" BerzerkerElementalDamageBaseIncr
      statLineIncr "Total Elemental Damage:" BerzerkerElementalDamageIncr
      statLine "Physical Damage:" BerzerkerPhysicalDamageBase BerzerkerPhysicalDamageBaseIncr
      statLineIncr "Total Physical Damage:" BerzerkerPhysicalDamageIncr
      statLine "Ice Damage:" BerzerkerIceDamageBase BerzerkerIceDamageBaseIncr
      statLineIncr "Total Ice Damage:" BerzerkerIceDamageIncr
      statLine "Fire Damage:" BerzerkerFireDamageBase BerzerkerFireDamageBaseIncr
      statLineIncr "Total Fire Damage:" BerzerkerFireDamageIncr
      statLine "Lightning Damage:" BerzerkerLightningDamageBase BerzerkerLightningDamageBaseIncr
      statLineFlat "Lightning Damage per Level:" BerzerkerLightningDamageBasePerLevel
      statLineIncr "Total Lightning Damage:" BerzerkerLightningDamageIncr
      statLine "Nemesis Damage:" BerzerkerNemesisDamageBase BerzerkerNemesisDamageBaseIncr
      statLineIncr "Total Nemesis Damage:" BerzerkerNemesisDamageIncr

      statLine "Maximum Health:" BerzerkerMaximumHealth BerzerkerMaximumHealthIncr
      statLineFlat "Health Regeneration:" BerzerkerHealthRegeneration
      statLineFlat "Elemental Resistance:" BerzerkerElementalResist
      statLineFlat "Physical Resistance:" BerzerkerPhysicalResist
      statLineFlat "Fire Resistance:" BerzerkerFireResist
      statLineFlat "Ice Resistance:" BerzerkerIceResist
      statLineFlat "Lightning Resistance:" BerzerkerLightningResist
      statLineFlat "Nemesis Resistance:" BerzerkerNemesisResist
      statLineFlat "Block Chance:" BerzerkerBlockChance
      statLineFlat "Dodge Chance:" BerzerkerDodgeChance
      statLineIncr "Movement Speed:" BerzerkerMovementSpeedIncr
    ]
    details [ Open true ] [
      summary [] [ str "Healer" ]
      statLine "Attack speed:" HealerAttackSpeed HealerAttackSpeedIncr
      statLineFlat "Critical Strike Chance:" HealerCriticalStrikeChance
      statLineFlat "Critical Strike Damage:" HealerCriticalStrikeDamage
      statLineIncr "Base Damage:" HealerDamageBaseIncr
      statLineIncr "Total Damage:" HealerDamageIncr
      statLineIncr "Base Elemental Damage:" HealerElementalDamageBaseIncr
      statLineIncr "Total Elemental Damage:" HealerElementalDamageIncr
      statLine "Physical Damage:" HealerPhysicalDamageBase HealerPhysicalDamageBaseIncr
      statLineIncr "Total Physical Damage:" HealerPhysicalDamageIncr
      statLine "Ice Damage:" HealerIceDamageBase HealerIceDamageBaseIncr
      statLineIncr "Total Ice Damage:" HealerIceDamageIncr
      statLine "Fire Damage:" HealerFireDamageBase HealerFireDamageBaseIncr
      statLineIncr "Total Fire Damage:" HealerFireDamageIncr
      statLine "Lightning Damage:" HealerLightningDamageBase HealerLightningDamageBaseIncr
      statLineFlat "Lightning Damage per Level:" HealerLightningDamageBasePerLevel
      statLineIncr "Total Lightning Damage:" HealerLightningDamageIncr
      statLine "Nemesis Damage:" HealerNemesisDamageBase HealerNemesisDamageBaseIncr
      statLineIncr "Total Nemesis Damage:" HealerNemesisDamageIncr

      statLine "Maximum Health:" HealerMaximumHealth HealerMaximumHealthIncr
      statLineFlat "Health Regeneration:" HealerHealthRegeneration
      statLineFlat "Elemental Resistance:" HealerElementalResist
      statLineFlat "Physical Resistance:" HealerPhysicalResist
      statLineFlat "Fire Resistance:" HealerFireResist
      statLineFlat "Ice Resistance:" HealerIceResist
      statLineFlat "Lightning Resistance:" HealerLightningResist
      statLineFlat "Nemesis Resistance:" HealerNemesisResist
      statLineFlat "Block Chance:" HealerBlockChance
      statLineFlat "Dodge Chance:" HealerDodgeChance
      statLineIncr "Movement Speed:" HealerMovementSpeedIncr
    ]
    details [ Open true ] [
      summary [] [ str "Ranger" ]
      statLine "Attack speed:" RangerAttackSpeed RangerAttackSpeedIncr
      statLineFlat "Critical Strike Chance:" RangerCriticalStrikeChance
      statLineFlat "Critical Strike Damage:" RangerCriticalStrikeDamage
      statLineIncr "Base Damage:" RangerDamageBaseIncr
      statLineIncr "Total Damage:" RangerDamageIncr
      statLineIncr "Base Elemental Damage:" RangerElementalDamageBaseIncr
      statLineIncr "Total Elemental Damage:" RangerElementalDamageIncr
      statLine "Physical Damage:" RangerPhysicalDamageBase RangerPhysicalDamageBaseIncr
      statLineIncr "Total Physical Damage:" RangerPhysicalDamageIncr
      statLine "Ice Damage:" RangerIceDamageBase RangerIceDamageBaseIncr
      statLineIncr "Total Ice Damage:" RangerIceDamageIncr
      statLine "Fire Damage:" RangerFireDamageBase RangerFireDamageBaseIncr
      statLineIncr "Total Fire Damage:" RangerFireDamageIncr
      statLine "Lightning Damage:" RangerLightningDamageBase RangerLightningDamageBaseIncr
      statLineFlat "Lightning Damage per Level:" RangerLightningDamageBasePerLevel
      statLineIncr "Total Lightning Damage:" RangerLightningDamageIncr
      statLine "Nemesis Damage:" RangerNemesisDamageBase RangerNemesisDamageBaseIncr
      statLineIncr "Total Nemesis Damage:" RangerNemesisDamageIncr

      statLine "Maximum Health:" RangerMaximumHealth RangerMaximumHealthIncr
      statLineFlat "Health Regeneration:" RangerHealthRegeneration
      statLineFlat "Elemental Resistance:" RangerElementalResist
      statLineFlat "Physical Resistance:" RangerPhysicalResist
      statLineFlat "Fire Resistance:" RangerFireResist
      statLineFlat "Ice Resistance:" RangerIceResist
      statLineFlat "Lightning Resistance:" RangerLightningResist
      statLineFlat "Nemesis Resistance:" RangerNemesisResist
      statLineFlat "Block Chance:" RangerBlockChance
      statLineFlat "Dodge Chance:" RangerDodgeChance
      statLineIncr "Movement Speed:" RangerMovementSpeedIncr
    ]
    details [ Open true ] [
      summary [] [ str "Enemy" ]
      statLineFlat "Block Chance:" EnemyBlockChance
      statLineFlat "Dodge Chance:" EnemyDodgeChance
      statLineIncr "Cast Time:" VoidInfluenceCastTimeIncr
    ]
    details [ Open true ] [
      summary [] [ str "Conversion" ]
      statLineFlat "Healpower -> Party Health Regeneration:" ConvHealpowerPartyHealthRegeneration
    ]
    details [ Open true ] [
      summary [] [ str "Skill" ]
      statLineIncr "Single Target Non-HOT Healing Skill Effect:" SingleTargetNonHotHealingSkillEffect
      statLineIncr "Buff Skill Effect:" BuffSkillEffect
      statLineIncr "Ample Heal Effect:" AmpleHealEffect
      // statLineFlat "AncientRootsGrant:" AncientRootsGrant
      // statLineFlat "AscensionGrant:" AscensionGrant
      statLineIncr "Angelic Infusion Less Cooldown" AngelicInfusionBlessedWingsCooldownLess
      statLineIncr "Angelic Infusion Mana Cost Grant" AngelicInfusionBlessedWingsManaCostIncr
      statLineFlat "Angelic Infusion Duration" AngelicInfusionDuration
      statLineIncr "Banish Attack Speed Grant:" BanishAttackSpeedIncr
      statLineFlat "Banish Cooldown:" BanishCooldown
      statLineFlat "Banish Elemental Resist Penalty:" BanishElementalResistPenalty
      statLineFlat "Blessing of Gaia Duration:" BlessingOfGaiaDuration
      statLineIncr "Blessing of Gaia Effect:" BlessingOfGaiaEffect
      statLineFlat "Blossom All Resistances Grant:" BlossomAllResist
      // statLineFlat "BlossomEnabled:" BlossomEnabled
      statLineFlat "Blossom Healpower Ratio:" BlossomHealpowerRatio
      statLineFlat "Cataclysm Damage Ratio:" CataclysmDamageRatio
      // statLineFlat "CataclysmEnabled:" CataclysmEnabled
      statLineFlat "Chaos Strike Cooldown On Berzerker Critical Strike:" ChaosStrikeCooldownOnBerzerkerCriticalStrike
      statLineIncr "Chaos Strike Damage:" ChaosStrikeDamageIncr
      statLineFlat "Chaos Strike Damage Ratio:" ChaosStrikeDamageRatio
      // statLineFlat "Chaos Strike Enabled:" ChaosStrikeEnabled
      statLineIncr "Cleansing Ray Attack Speed Grant:" CleansingRayAttackSpeedIncr
      statLineIncr "Cleansing Ray Base Damage Grant:" CleansingRayBaseDamageIncr
      statLineFlat "Cleansing Ray Ice Damage Ratio:" CleansingRaySelfDamage
      statLineIncr "Cleansing Ray Judgement Ratio:" CleansingRayJudgementRatio
      statLineIncr "Cleansing Ray Mana Cost:" CleansingRayManaCostIncr
      statLineIncr "Cleansing Ray Zeal Base Damage:" CleansingRayZealBaseDamageIncr
      statLineIncr "Cleansing Ray Zeal Attack Speed:" CleansingRayZealAttackSpeedIncr
      // statLineIncr "CleansingRayZealEnabled:" CleansingRayZealEnabled
      statLineIncr "Dispel Cooldown:" DispelCooldownIncr
      // statLineFlat "DivineFocusBeginsCooldownImmediately:" DivineFocusBeginsCooldownImmediately
      statLineFlat "Divine Focus Cooldown:" DivineFocusCooldown
      statLineIncr "Divine Mend Effect:" DivineMendEffect
      statLineIncr "Divine Mend Effect per Redemption Stack:" DivineMendRedemptionEffect
      statLineFlat "Divine Mend Redemption Max Stacks:" DivineMendRedemptionMaxStacks
      statLineFlat "Flash Heal Cooldown:" FlashHealCooldown
      statLineIncr "Flash Heal Effect:" FlashHealEffect
      statLineFlat "Flash Heal Infused Luminosity Chance:" FlashHealInfusedLuminosityChance
      statLineIncr "Flash Heal Infused Luminosity Healing:" FlashHealInfusedLuminosityHealingMore
      statLineFlat "Flash Heal Infused Luminosity Damage Ratio:" FlashHealInfusedLuminosityDamageRatio
      statLineFlat "Flash Heal Infused Luminosity All Resist Penalty:" FlashHealInfusedLuminosityAllResistPenalty
      statLineIncr "Flash Heal Mana Cost:" FlashHealManaCostIncr
      statLineFlat "Hammer Of Reckoning Charges:" HammerOfReckoningCharges
      statLineIncr "Hammer Of Reckoning Effect:" HammerOfReckoningEffect
      statLineFlat "Holy Shield Cooldown:" HolyShieldCooldown
      statLineFlat "Holy Shield Duration:" HolyShieldDuration
      statLineIncr "Holy Shield Effect:" HolyShieldEffect
      // statLineFlat "InspireEnabled:" InspireEnabled
      statLineIncr "Inspire Party Damage:" InspirePartyDamageIncr
      statLineFlat "Inspire Regeneration:" InspireRegeneration
      statLineFlat "Inspire Healpower -> Regeneration:" InspireRegenerationRatio
      statLineFlat "Heaven's Melody Cooldown" HeavensMelodyCooldown
      statLineFlat "Heaven's Melody Cooldown on Radiant Heal" HeavensMelodyCooldownOnRadiantHeal
      statLineFlat "Iron Vines Duration:" IronVinesDuration
      statLineIncr "Iron Vines Healpower Grant:" IronVinesHealpowerIncr
      statLineIncr "Lesser Heal Effect" LesserHealEffect
      statLineIncr "Lesser Heal Extra Target Healing Ratio" LesserHealExtraTargetRatio
      statLineIncr "Mind Rush Attack Speed:" MindRushAttackSpeedIncr
      statLineFlat "Mind Rush Cast Time:" MindRushCastTime
      statLineFlat "Mind Rush Duration:" MindRushDuration
      statLineFlat "Mind Rush Nemesis Resistance:" MindRushNemesisResist
      // statLineFlat "NaturesGiftCanCrit:" NaturesGiftCanCrit
      statLineIncr "Nature's Gift Cooldown:" NaturesGiftCooldownIncr
      statLineIncr "Nature's Gift Effect:" NaturesGiftEffect
      statLineFlat "Radiant Heal Cast Time" RadiantHealCastTime
      statLineFlat "Radiant Heal Cooldown" RadiantHealCooldown
      statLineIncr "Radiant Heal Effect" RadiantHealEffect
      statLineIncr "Radiant Heal Shield Ratio" RadiantHealShieldRatio
      statLineFlat "Radiant Heal Trail of Fate Ratio" RadiantHealTrailOfFateRatio
      statLineFlat "Rapid Shot Cast Time:" RapidShotCastTime
      statLineFlat "Rapid Shot Cooldown:" RapidShotCooldown
      // statLineFlat "Rapid Shot Enabled:" RapidShotEnabled
      statLineIncr "Rapid Shot Damage:" RapidShotDamageIncr
      statLineFlat "Rapid Shot Damage Ratio:" RapidShotDamageRatio
      statLineFlat "Rapid Shot Arrows:" RapidShotArrows
      statLineIncr "Rapture Damage Ratio" RaptureRatio
      statLineIncr "Reinforced Barrier Chance" ReinforcedBarrierChance
      statLineIncr "Reinforced Barrier Ratio" ReinforcedBarrierMaximumHealthRatio
      statLineIncr "Rejuvenate Cooldown:" RejuvenateCooldownIncr
      statLineFlat "Rejuvenate Cooldown on Healer Crit:" RejuvenateCooldownOnHealerCrit
      statLineFlat "Rejuvenate Cooldown on Spirit Ritual Tick:" RejuvenateCooldownOnSpiritRitualTick
      statLineFlat "Rejuvenate Heal Ratio:" RejuvenateHealRatio
      statLineFlat "Rejuvenate Mana Per Tick:" RejuvenateManaPerTick
      statLine "Rejuvenate Party Ice Damage Base:" RejuvenatePartyIceDamageBase RejuvenatePartyIceDamageBaseIncr
      // statLineFlat "RenewCanCrit:" RenewCanCrit
      statLine "Renew Cooldown:" RenewCooldown RenewCooldownIncr
      statLineFlat "Renew Duration:" RenewDuration
      statLineIncr "Renew Effect:" RenewEffect
      statLineIncr "Renew Focused Glory Healing:" RenewFocusedGloryHealingMore
      statLineFlat "Renew Mana Restore Chance:" RenewManaRestoreChance
      statLineIncr "Restoration Totem Base Fire Damage:" RestorationTotemFireDamageBaseIncr
      statLineIncr "Restoration Totem Base Ice Damage:" RestorationTotemIceDamageBaseIncr
      statLineIncr "Restoration Totem Base Lightning Damage:" RestorationTotemLightningDamageBaseIncr
      statLineFlat "Restoration Totem Charges:" RestorationTotemCharges
      statLineIncr "Restoration Totem Cooldown:" RestorationTotemCooldownIncr
      statLineIncr "Restoration Totem Damage Taken Reduction:" RestorationTotemDamageTakenReduc
      statLineIncr "Restoration Totem Effect:" RestorationTotemEffect
      statLineIncr "Restoration Totem Healing Received:" RestorationTotemHealingReceivedIncr
      // statLineFlat "ReviveGrant:" ReviveGrant
      statLineFlat "Sacred Infusion Elemental Resist:" SacredInfusionElementalResist
      statLineFlat "Sacred Infusion Health Regeneration:" SacredInfusionHealthRegeneration
      statLineFlat "Sacred Infusion Physical Resist:" SacredInfusionPhysicalResist
      statLineIncr "Salvation Damage Taken Reduction:" SalvationDamageTakenReduc
      statLineIncr "Salvation Fire/Lightning Damage:" SalvationFireLightningDamageIncr
      statLineFlat "Sanctuary Duration:" SanctuaryDuration
      statLineFlat "Sanctuary Cooldown:" SanctuaryCooldown
      // statLineFlat "SealOfLightCanCritHeal:" SealOfLightCanCritHeal
      // statLineFlat "SealOfLightCanCrit:" SealOfLightCanCrit
      statLineIncr "Seal Of Light Effect:" SealOfLightEffect
      statLineFlat "Seal Of Light Max Stacks:" SealOfLightMaxStacks
      statLineFlat "Shadow Ripple Effect:" ShadowRippleEffect
      statLineFlat "Shadow Ripple Critical Heal Chance:" ShadowRippleCriticalHealChance
      statLineFlat "Shadow Ripple Repose of the Arcane Ratio:" ShadowRippleOverhealRatio
      statLineIncr "Siphon Life Damage Grant:" SiphonLifeDamageIncr
      statLineFlat "Siphon Life Duration:" SiphonLifeDuration
      statLineFlat "Siphon Life Effect:" SiphonLifeEffect
      statLineFlat "Siphon Life Extra Healing Targets:" SiphonLifeExtraHealingTargets
      statLineIncr "Siphon Life Healing:" SiphonLifeHealingIncr
      statLineFlat "Smite Cooldown:" SmiteCooldown
      statLineFlat "Smite Cooldown on Healer Crit:" SmiteCooldownOnHealerCrit
      statLineIncr "Smite Damage:" SmiteDamageIncr
      statLineIncr "Smite Damage with Twist of Fate:" SmiteTwistOfFateDamageIncr
      statLineIncr "Spirit Orb Effect" SpiritOrbEffect
      statLineFlat "Spirit Orb Ice Damage Heal Ratio:" SpiritOrbIceDamageHealRatio
      statLineFlat "Spirit Orb Ice Resistance Debuff:" SpiritOrbIceResistDebuff
      // statLineFlat "SpiritRitualCanCrit:" SpiritRitualCanCrit
      statLineIncr "Spirit Ritual Effect:" SpiritRitualEffect
      statLineIncr "Spirit Ritual Healing Received:" SpiritRitualHealingReceivedIncr
      statLineFlat "Spirit Ritual Refresh Chance:" SpiritRitualRefreshChance
      statLineIncr "Star Call Damage:" StarCallDamageIncr
      statLineFlat "Star Call Splash Chance:" StarCallSplashChance
      statLineFlat "Star Call Splash Damage Ratio:" StarCallSplashDamageRatio
      statLineIncr "Toughen Damage Taken:" ToughenDamageTakenIncr
      statLineFlat "Toughen Duration:" ToughenDuration
      statLineIncr "Toughen Effect:" ToughenEffect
      // statLineFlat "VitalizeGrant:" VitalizeGrant
      statLineFlat "Void Zone Charges:" VoidZoneCharges
      statLineFlat "Void Zone Shadow Entropy Damage Ratio:" VoidZoneShadowEntropyDamageRatio
    ]
    details [ Open true ] [
      summary [] [ str "Miscellaneous" ]
      statLineIncr "Astral Shift (healpower/cast speed):" AstralShift
      statLineFlat "Blessed Company Tank Physical Resist:" BlessedCompanyTankPhysicalResist
      statLineFlat "Blessed Company Tank Duration:" BlessedCompanyTankDuration
      statLineFlat "Blessed Company Berzerker/Ranger Damage Ratio:" BlessedCompanyBerzerkerRangerDamageRatio
      statLineFlat "Blessed Company Heal Ratio:" BlessedCompanyHealerHealRatio
      statLineFlat "Blessing of the Forest Missing Health Healed:" BlessingOfTheForestHealingRatio
      statLineFlat "Conviction Chance:" ConvictionChance
      statLineFlat "Conviction Damage Ratio:" ConvictionDamageRatio
      statLineIncr "Conviction Damage Granted:" ConvictionDamageGrantReduc
      // statLineFlat "ConvocationChannelRejuvenate:" ConvocationChannelRejuvenate
      statLineFlat "Decay Aura Damage Ratio:" DecayAuraDamageRatio
      statLineIncr "Decay Aura Damage Taken Grant:" DecayAuraDamageTakenIncr
      statLineFlat "Deception All Resists Bonus:" DeceptionPartyAllResistsBonus
      statLineFlat "Deception All Resists Penalty:" DeceptionPartyAllResistsPenalty
      statLineFlat "Divine Aegis Heal Ratio:" DivineAegisHealRatio
      statLineFlat "Divine Decree Block/Dodge Time:" DivineDecreeBlockDodgeTimer
      statLineFlat "Divine Decree Healing Time:" DivineDecreeHealingTimer
      statLineFlat "Spell Cast Refund Chance:" DeepMindRefundChance
      statLineFlat "Devotion Block Chance:" DevotionBlockChance
      statLineIncr "Devotion Damage:" DevotionDamageIncr
      // statLineFlat "DevotionEnabled:" DevotionEnabled
      statLineFlat "Devotion Physical/Nemesis Resistance:" DevotionPhysicalNemesisResist
      statLineIncr "Divine Purpose Healpower:" DivinePurposeHealpowerIncr
      statLineIncr "Eradication Damage Taken:" EradicationDamageTakenMore
      statLineIncr "Feathered Footwork Mana Cost:" FeatheredFootworkManaCostIncr
      statLineIncr "Feathered Footwork Move Speed:" FeatheredFootworkMoveSpeedReduc
      // statLineFlat "GlimmerOfHopeEnabled:" GlimmerOfHopeEnabled
      statLineFlat "Heaven's Aid Shield Ratio:" HeavensAidShieldRatio
      // statLineFlat "HeightenedSensesEnabled:" HeightenedSensesEnabled
      statLineFlat "Healpower per Heightened Senses Stack:" HeightenedSensesHealpower
      statLineFlat "Heroism Redirect Ratio:" HeroismRedirectRatio
      statLineFlat "Immolate Healpower -> Damage" ImmolateRatio
      statLineFlat "Base Ice Damage per Heightened Senses Stack:" HeightenedSensesIceDamageBase
      statLineIncr "Immovable Fortress Slow Reduction:" ImmovableFortressSlowEffectReduce
      statLineIncr "Immovable Fortress Silence Duration:" ImmovableFortressSilenceDurationReduce
      statLineIncr "Immovable Fortress Damage Taken:" ImmovableFortressDamageTakenIncr
      statLineFlat "Inquisition Cooldown Modifier:" InquisitionCooldownMod
      // statLineIncr "IntoTheFrayEnabled:" IntoTheFrayEnabled
      statLineIncr "Last Stand Damage Taken:" LastStandDamageTakenLess
      statLineIncr "Last Stand Damage:" LastStandDamageMore
      // statLineFlat "LeaveNoOneBehindEnabled:" LeaveNoOneBehindEnabled
      statLineIncr "Light's Guidance Damage Taken Reduction:" LightsGuidanceDamageTakenReduc
      statLineFlat "Mark Of Death Chance:" MarkOfDeathChance
      statLineFlat "Mark Of Death Damage Ratio:" MarkOfDeathDamageRatio
      statLineIncr "Mark Of Death Damage Taken Ratio:" MarkOfDeathDamageTakenIncrRatio
      statLineFlat "Nemesis Infusion Chance:" NemesisInfusionChance
      statLineIncr "Path's End Healpower:" PathsEndHealpowerIncr
      statLineFlat "Poison Chance:" PoisonChance
      statLineFlat "Ranger Critical Strike Stats -> Poison Critical Strike Stats:" PoisonCriticalStrikeChanceRatio
      statLine "Poison Damage:" PoisonDamage PoisonDamageIncr
      statLineFlat "Ranger Base Damage -> Poison Damage:" PoisonDamageRatio
      statLineFlat "Maximum Poison Stacks:" PoisonMaxStacks
      statLineFlat "Pure In Heart Ratio:" PureInHeartRatio
      statLineFlat "Reactive Instinct Dodge Chance:" ReactiveInstinctDodgeChance
      statLineIncr "Ring Of Influence Healing Received:" RingOfInfluenceHealingReceivedIncr
      // statLineFlat "ShadeEnabled:" ShadeEnabled
      statLineFlat "Shade Maximum Health -> Regeneration:" ShadeRegenerationRatio
      statLineFlat "Shield Bash Damage Ratio:" ShieldBashDamageRatio
      // statLineFlat "ShieldBashEnabled:" ShieldBashEnabled
      statLineIncr "Soul Boost Physical Damage Grant:" SoulBoostPhysicalDamageIncr
      statLineIncr "Soul Boost Fire Damage Grant:" SoulBoostFireDamageIncr
      statLineFlat "Survival Instinct Dodge Chance:" SurvivalInstinctDodgeChance
      // statLineFlat "SurvivalInstinctEnabled:" SurvivalInstinctEnabled
      statLineIncr "Survival Instinct Healing Received:" SurvivalInstinctHealingReceived
      statLineFlat "Sword and Board Paladin Skill Levels:" SwordAndBoardPaladinSkillLevels
      statLineFlat "Taunt Less Damage:" TauntDamageLess
      statLineFlat "Taunt Damage Taken:" TauntDamageTakenMore
      // statLineFlat "TauntEnabled:" TauntEnabled
      statLineFlat "Thunder Assault Lightning Damage:" ThunderAssaultDamage
      statLineFlat "Thunder Assault Damage Ratio:" ThunderAssaultDamageRatio
      // statLineFlat "ThunderAssaultEnabled:" ThunderAssaultEnabled
      statLineFlat "Time in Need Healing Ratio:" TimeInNeedHealingRatio
      statLineIncr "Typhoon Attack Speed Slow:" TyphoonAttackSpeedSlow
      // statLineFlat "TyphoonEnabled:" TyphoonEnabled
      statLineIncr "Typhoon Physical and Ice Damage Taken:" TyphoonPhysicalIceDamageTakenIncr
      statLineFlat "Vampirism—Healing (Berzerker):" VampirismBerzerker
      statLineFlat "Vampirism—Healing (Healer):" VampirismHealer
      statLineFlat "Vampirism—Healing (Ranger):" VampirismRanger
      statLineFlat "Vampirism—Healing (Tank):" VampirismTank
      statLineFlat "Vampirism—Healing Ratio:" VampirismRatio
      statLineIncr "Void Chains—Healing Received:" VoidChainsHealingReceivedIncr
      statLineFlat "Voodoo Magic—Health Restored:" VoodooMagicRatio
      statLineFlat "Wicked Bloom—Renew Chance:" WickedBloomRenewChance
      statLineFlat "Wicked Bloom—Shadow Ripple Chance:" WickedBloomShadowRippleChance
      statLineFlat "Worship Shield Ratio:" WorshipShieldRatio
    ]
    details [ Open true ] [
      summary [] [
        str "Per Point Sacrificed"
      ]
      // statLineFlat "Points Sacrificed:" Sacrifice
      statLineIncr "Banish—Attack Speed Grant:" BanishSacrificeAttackSpeedRatioIncr
      statLineIncr "Cataclysm—Damage:" CataclysmSacrificeDamageMore
      statLineIncr "Cleansing Ray—Base Damage Grant:" CleansingRaySacrificeBaseDamageIncr
      statLineFlat "Decay Aura—Healer Damage:" DecayAuraSacrificeRatio
      statLineIncr "Eradication—Damage Taken:" EradicationDamageSacrificeRatio
      statLineFlat "Nemesis Infusion—Proc Chance:" NemesisInfusionSacrificeRatio
      statLineIncr "Berzerker Attack Speed:" SacrificeBerzerkerAttackSpeedIncr
      statLineIncr "Healpower:" SacrificeHealpowerIncr
      statLineFlat "Maximum Mana:" SacrificeMaximumMana
      statLineFlat "Party Block Chance:" SacrificePartyBlockChance
      statLineFlat "Party Dodge Chance:" SacrificePartyDodgeChance
      statLineIncr "Party Maximum Health:" SacrificePartyMaximumHealthIncr
      statLineFlat "Siphon Life Damage Grant:" SiphonLifeSacrificeDamageIncr
      statLineFlat "Shadow Ripple Repose of the Arcane Ratio::" ShadowRippleSacrificeOverhealRatio
      statLineFlat "Vampirism—Healing Ratio:" VampirismSacrificeRatio
      statLineFlat "Void Chains—Healing Received:" VoidChainsSacrificeHealingReceivedIncr
      statLineFlat "Void Zone Shadow Entropy Damage Ratio:" VoidZoneShadowSacrificeEntropyDamageRatio
      statLineFlat "Wicked Bloom—Proc Chance:" WickedBloomSacrificeChance
    ]
  ]
