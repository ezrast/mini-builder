module Tabber

open Fable.React
open Fable.React.Props
open Msg

let render (tabs: List<Msg.Class * ReactElement * ReactElement>) active dispatch =
  let folder (label, tab, element) (tabRow, content) =
    let tabClass, contentClass =
      if active = label then
        "tabber-tab tabber-tab-active", "tabber-content"
      else
        "tabber-tab tabber-tab-inactive", "tabber-content hidden"

    let newTabRow =
      button [ Class tabClass; OnMouseDown(fun _evt -> dispatch (SetTab label)) ] [
        tab
      ]
      :: tabRow
    let newContent = div [ Class contentClass ] [ element ] :: content
    (newTabRow, newContent)

  let tabRowItems, content = List.foldBack folder tabs ([], [])
  let tabRow: ReactElement = div [ Class "tabber-tabrow" ] tabRowItems
  div [ Class "tabber" ] (tabRow :: content)
